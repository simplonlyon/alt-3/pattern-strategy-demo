package co.simplon.alt3;

import co.simplon.alt3.authenticators.APIAuthenticator;
import co.simplon.alt3.authenticators.LDAPAuthenticator;
import co.simplon.alt3.authenticators.SSOAuthenticator;
import co.simplon.alt3.entities.User;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Démonstration du pattern Strategy" );
        User u = new User("Barth", "Simplon1234");

        u.connect();
        u.setAuth(new LDAPAuthenticator());
        u.connect();
        u.setAuth(new SSOAuthenticator());
        u.connect();
        u.setAuth(new APIAuthenticator());
        u.connect();
        // Exemple avec une Factory
        // public IAuthenticator getAuthenticator()
        //u.setAuth(AuthenticatorFactory.getAuthenticator());
    }
}
