package co.simplon.alt3.authenticators;

import co.simplon.alt3.entities.User;

public class DBAuthenticator implements IAuthenticator {
    public boolean connect(User u) {
        System.out.println("Je connecte l'utilisateur " + u.getName() + " via la BDD");
        return true;
    }
}
