package co.simplon.alt3.authenticators;

import co.simplon.alt3.entities.User;

public interface IAuthenticator {
    public boolean connect(User u);
}
