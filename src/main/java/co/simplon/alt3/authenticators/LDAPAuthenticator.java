package co.simplon.alt3.authenticators;

import co.simplon.alt3.entities.User;

public class LDAPAuthenticator implements IAuthenticator {
    public boolean connect(User u) {
        System.out.println("Je connecte l'utilisateur " + u.getName() + " via le LDAP");
        return true;
    }
    
}
