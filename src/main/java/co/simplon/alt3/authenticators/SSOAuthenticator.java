package co.simplon.alt3.authenticators;

import co.simplon.alt3.entities.User;

public class SSOAuthenticator implements IAuthenticator {
    public boolean connect(User u) {
        System.out.println("Je connecte l'utilisateur " + u.getName() + " via le SSO");
        return true;
    }
    
}
