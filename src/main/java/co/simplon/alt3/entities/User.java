package co.simplon.alt3.entities;

import co.simplon.alt3.authenticators.DBAuthenticator;
import co.simplon.alt3.authenticators.IAuthenticator;

public class User {
    private String name;
    private String password;
    private IAuthenticator auth = new DBAuthenticator();
    /**
     * @param name
     * @param password
     */
    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }
    /**
     * 
     */
    public User() {
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean connect() {
        this.auth.connect(this);
        return true;
    }

    /**
     * @param auth the auth to set
     */
    public void setAuth(IAuthenticator auth) {
        this.auth = auth;
    }
    
}
